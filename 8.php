<?php
$letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

// Inicializar varaibles
$dniNumero = 0;
$dniLetra = "";

// Si se ha pulsado el botón de enviar
if (isset($_GET["enviar"])) {
    // Guaradamos el valor del DNI introducido por el usuario
    $dniNumero = $_GET["dniNumero"];
    $dniLetra = $_GET["letra"];
    //comprobamos que el numero sea correcto
    if ($dniNumero > 0 && $dniNumero < 99999999) {
        //Calculamos la letra en fucnión al numero del DNI introducido
        $posicionArrayLetras = $dniNumero % 23;
        $letraCalculada = $letras[$posicionArrayLetras];

        // Comprobamos que la letra introducida sea correcta
        if ($letraCalculada == $dniLetra) {
            echo "El número y la letra introducidos son corectos";
        } else {
            echo "La letra introducida no es correcta";
        }
    } else {
        // Si el numero no es correcto
        echo "El número proporcionado no es válido";
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- Formulario que te pide el DNI y la letra-->
    <form action="">
        <div>
            <label for="dniNumero">DNI (Números)</label>
            <input type="text" name="dniNumero" id="dniNumero">
            <label for="letra">Letra</label>
            <input type="text" name="letra" id="letra">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>
</body>

</html>